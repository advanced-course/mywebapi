﻿using Microsoft.EntityFrameworkCore;

namespace MyWebApi
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Post> Posts { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasKey(p => p.Id);
            modelBuilder.Entity<Post>().Property(p => p.Id).UseIdentityAlwaysColumn();
        }
    }

    public class Post
    {
        /// <summary>
        /// รหัสโพสต์
        /// </summary>
        /// <example>1</example>
        public int Id { get; set; }
        /// <summary>
        /// ชื่อโพสต์
        /// </summary>
        /// <example>หัวข้อ</example>
        public string Title { get; set; }
        /// <summary>
        /// เนื้อหาโพสต์
        /// </summary>
        /// <example>เนื้อหา</example>
        public string Body { get; set; }
    }

    public class PostDto
    {
        /// <summary>
        /// ชื่อโพสต์
        /// </summary>
        /// <example>หัวข้อ</example>
        public string Title { get; set; }
        /// <summary>
        /// เนื้อหาโพสต์
        /// </summary>
        /// <example>เนื้อหา</example>
        public string Body { get; set; }
    }
}
