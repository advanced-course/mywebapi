﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyWebApi;

namespace MyWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public PostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// แสดงรายการโพสต์ทั้งหมด
        /// </summary>
        /// <returns>รายการโพสต์ทั้งหมด</returns>
        /// <response code="200">รายการโพตส์</response>
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Post>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Post>>> GetPosts()
        {
            return await _context.Posts.ToListAsync();
        }

        /// <summary>
        /// แสดงรายการโพสต์ตามรหัส
        /// </summary>
        /// <param name="id">รหัสโพสต์</param>
        /// <returns>รายการโพสต์ตามรหัส</returns>
        /// <response code="200">รายการโพสต์</response>
        /// <response code="404">ไม่พบโพสต์</response>
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ActionResult<Post>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<Post>> GetPost(int id)
        {
            var post = await _context.Posts.FindAsync(id);

            if (post == null)
            {
                return NotFound();
            }

            return post;
        }

        /// <summary>
        /// สร้างโพสต์ใหม่
        /// </summary>
        /// <param name="Title">หัวข้อเรื่อง</param>
        /// <param name="Body">เนื้อหาของโพสต์</param>
        /// <returns>ข้อมูลโพสต์</returns>
        /// <response code="201">สร้างโพสต์สำเร็จ</response>
        /// <response code="400">ข้อมูลไม่ถูกต้อง</response>
        [HttpPost]
        public async Task<ActionResult<Post>> PostPost(PostDto post)
        {
            var newPost = new Post()
            {
                Title = post.Title,
                Body = post.Body
            };
            _context.Posts.Add(newPost);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPost", new { id = newPost.Id }, newPost);
        }
    }
}
